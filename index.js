var autocss = require('autocss')
var Transform = require('readable-stream/transform')

module.exports = function() {
  return new Transform({
    objectMode: true,
    transform: function(file, encoding, callback) {
      var error = null
      var contents = String(file.contents)
      var regex = /\sclass\=\"([^\"]+)/g
      var classes = []
      var match = null
      while(match = regex.exec(contents)) {
        Array.prototype.push.apply(classes, match[1].split(/\s+/))
      }
      var css = autocss.css(classes)
      file.contents = new Buffer(css)
      this.push(file)
      return callback()
    }
  })
}
